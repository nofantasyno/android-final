package com.example.finalproject.loader

import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.Result
import com.example.finalproject.api.model.Recommendations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecomendationsLoader(var mCallback: RecommendationsLoadCallback) {
    fun loadRecommendedMovies(id: String) {
        MovieService.getApi()
            .getMovieRecommendations(id)
            .enqueue(object : Callback<Recommendations> {
                override fun onResponse(
                    call: Call<Recommendations>,
                    response: Response<Recommendations>
                ) {
                    mCallback.onRecommendationsLoaded(response.body()!!.results!!)
                }

                override fun onFailure(call: Call<Recommendations>, t: Throwable) {

                }
            })
    }

    fun loadSimilarMovies(id: String) {
        MovieService.getApi()
            .getSimilarMovies(id)
            .enqueue(object : Callback<Recommendations> {
                override fun onResponse(
                    call: Call<Recommendations>,
                    response: Response<Recommendations>
                ) {
                    mCallback.onSimilarLoaded(response.body()!!.results!!)
                }

                override fun onFailure(call: Call<Recommendations>, t: Throwable) {

                }
            })
    }

    interface RecommendationsLoadCallback {

        fun onRecommendationsLoaded(movies: List<Result>)

        fun onSimilarLoaded(movies: List<Result>)
    }
}