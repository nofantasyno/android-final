package com.example.finalproject.loader

import android.util.Log
import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.Genre
import com.example.finalproject.api.model.Genres
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreLoader(var mCallback: GenreLoadCallback){
    fun loadGenres() {
        MovieService.getApi()
            .getGenreList()
            .enqueue(object : Callback<Genres> {
                override fun onResponse(call: Call<Genres>, response: Response<Genres>) {
                    mCallback.onGenresLoaded(response.body()!!.genres!!)
                }

                override fun onFailure(call: Call<Genres>, t: Throwable) {

                }
            })
    }

    interface GenreLoadCallback {
        fun onGenresLoaded(genres: ArrayList<Genre>)
    }
}