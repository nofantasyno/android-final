package com.example.finalproject.loader

import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesLoader(callback: MovieLoadCallback) {
    var mCallback: MovieLoadCallback = callback

    fun loadMovieDetails(id: String, language: String) {
        MovieService.getApi()
            .getMovieDetails(id, language)
            .enqueue(object : Callback<Movie> {
                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    mCallback.onMovieDetailsLoaded(response.body())
                }

                override fun onFailure(call: Call<Movie>, t: Throwable) {

                }
            })
    }


    fun loadPopularMovies(genre_id: Int, language: String) {
        MovieService.getApi()
            .getPopularMovies(language)
            .enqueue(object : Callback<Movies> {
                override fun onResponse(call: Call<Movies>, response: Response<Movies>) {
                    mCallback.onMoviesLoaded(response.body()!!.results!!, genre_id)
                }

                override fun onFailure(call: Call<Movies>, t: Throwable) {

                }
            })
    }

    fun loadLatestMovie() {
        MovieService.getApi()
            .getLatestMovie()
            .enqueue(object : Callback<Movie> {
                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    mCallback.onMovieDetailsLoaded(response.body())
                }

                override fun onFailure(call: Call<Movie>, t: Throwable) {

                }
            })
    }

    fun loadNowPlayingMovies(genre_id: Int, language: String) {
        MovieService.getApi()
            .getNowPlayingMovies(language)
            .enqueue(object : Callback<Movies> {
                override fun onResponse(call: Call<Movies>, response: Response<Movies>) {
                    mCallback.onMoviesLoaded(response.body()!!.results!!, genre_id)
                }

                override fun onFailure(call: Call<Movies>, t: Throwable) {

                }
            })
    }

    fun loadTopRatedMovies(genre_id: Int, language: String) {
        MovieService.getApi()
            .getTopRatedMovies(language)
            .enqueue(object : Callback<Movies> {
                override fun onResponse(call: Call<Movies>, response: Response<Movies>) {
                    mCallback.onMoviesLoaded(response.body()!!.results!!, genre_id)
                }

                override fun onFailure(call: Call<Movies>, t: Throwable) {

                }
            })
    }


    fun loadUpcomingMovies(genre_id: Int, language: String) {
        MovieService.getApi()
            .getUpcomingMovies(language)
            .enqueue(object : Callback<Movies> {
                override fun onResponse(call: Call<Movies>, response: Response<Movies>) {
                    mCallback.onMoviesLoaded(response.body()!!.results!!, genre_id)
                }

                override fun onFailure(call: Call<Movies>, t: Throwable) {

                }
            })
    }

    fun loadMovieVideos(movie_id: String) {
        MovieService.getApi()
            .getMovieVideos(movie_id)
            .enqueue(object : Callback<Videos> {
                override fun onResponse(call: Call<Videos>, response: Response<Videos>) {
                    mCallback.onMovieVideosLoaded(response.body()!!.results!!)
                }

                override fun onFailure(call: Call<Videos>, t: Throwable) {

                }
            })
    }

    fun loadCastDetails(id: String) {
        MovieService.getApi()
            .getCastDetails(id)
            .enqueue(object : Callback<Casts> {
                override fun onResponse(call: Call<Casts>, response: Response<Casts>) {
                    mCallback.onCastDetailsLoaded(response.body()!!.cast!!)
                }

                override fun onFailure(call: Call<Casts>, t: Throwable) {

                }
            })
    }


    interface MovieLoadCallback {
        fun onMoviesLoaded(movies: List<Result>, genre_id: Int)

        fun onMovieDetailsLoaded(movieDetails: Movie?)

        fun onMovieVideosLoaded(videoResult: List<Video>)

        fun onCastDetailsLoaded(casts: List<Cast>)
    }

}
