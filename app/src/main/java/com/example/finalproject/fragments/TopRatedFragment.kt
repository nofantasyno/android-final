package com.example.finalproject.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.finalproject.R
import com.example.finalproject.adapter.MoviesAdapter
import com.example.finalproject.api.model.Cast
import com.example.finalproject.api.model.Movie
import com.example.finalproject.api.model.Video
import com.example.finalproject.loader.MoviesLoader
import java.util.ArrayList

class TopRatedFragment: Fragment(), MoviesLoader.MovieLoadCallback {

    override fun onMovieDetailsLoaded(movieDetails: Movie?) {

    }

    override fun onMovieVideosLoaded(videoResult: List<Video>) {

    }

    override fun onCastDetailsLoaded(casts: List<Cast>) {

    }

    private var mLoader: MoviesLoader? = null
    private var mMoviesView: RecyclerView? = null
    private var mMoviesAdapter: MoviesAdapter? = null


    internal lateinit var view: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.top_rated_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mMoviesView = getView()!!.findViewById(R.id.movies_list_top)
        mLoader = MoviesLoader(this)
        mMoviesView!!.layoutManager = GridLayoutManager(activity, 1)
        mMoviesAdapter = MoviesAdapter(this.context!!)
        mMoviesView!!.adapter = mMoviesAdapter

        getView()!!.findViewById<View>(R.id.progress_bar_top).visibility = View.VISIBLE

        val language = this.activity!!
            .getSharedPreferences(
                getString(R.string.app_name),
                Context.MODE_PRIVATE
            ).getString("language", "en-EN")

        if (activity!!.intent.hasExtra("genre_id")) {
            mLoader!!.loadTopRatedMovies(Integer.parseInt(activity!!.intent.getStringExtra("genre_id")), language!!)
        } else {
            mLoader!!.loadTopRatedMovies(0, language!!)
        }
    }

    override fun onMoviesLoaded(movies: List<com.example.finalproject.api.model.Result>, genre_id: Int) {
        getView()!!.findViewById<View>(R.id.progress_bar_top).visibility = View.GONE
        if (genre_id == 0) {
            mMoviesAdapter!!.setMovies(movies)
        } else {
            val mSortedMovies = ArrayList<com.example.finalproject.api.model.Result>()
            for (i in movies.indices) {
                if (movies[i].genreIds!!.contains(genre_id)) {
                    mSortedMovies.add(movies[i])
                }
            }

            mMoviesAdapter!!.setMovies(mSortedMovies)
        }
    }



}