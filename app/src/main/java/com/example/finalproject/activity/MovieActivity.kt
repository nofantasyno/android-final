package com.example.finalproject.activity


import android.content.Context
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

import com.example.finalproject.R
import com.example.finalproject.adapter.*
import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.Cast
import com.example.finalproject.api.model.Movie
import com.example.finalproject.api.model.Result
import com.example.finalproject.api.model.Video
import com.example.finalproject.loader.MoviesLoader
import com.example.finalproject.loader.RecomendationsLoader
import com.squareup.picasso.Picasso

class MovieActivity : AppCompatActivity(), MoviesLoader.MovieLoadCallback,
    RecomendationsLoader.RecommendationsLoadCallback {

    var mToolbar: android.support.v7.widget.Toolbar? = null
    var mRecyclerView: RecyclerView? = null
    var mRecyclerViewSimilar: RecyclerView? = null
    var mLogosRecyclerView: RecyclerView? = null
    var mVideosRecyclerView: RecyclerView? = null
    var mRecyclerViewCredits: RecyclerView? = null
    var mImageView: ImageView? = null
    var mItemSlogan: TextView? = null
    var mItemOverview: TextView? = null
    var mItemTitle: TextView? = null
    var mItemReleaseDate: TextView? = null
    var mItemGenre: TextView? = null
    var mOriginalTitle: TextView? = null
    var mBudget: TextView? = null
    var mRevenue: TextView? = null
    var mRuntime: TextView? = null
    var mAvgVote: TextView? = null
    var mLoader: MoviesLoader? = null
    var mRecommendationsLoader: RecomendationsLoader? = null
    var mProgressBar: ProgressBar? = null
    var mLogosAdapter: LogosAdapter? = null
    var mRecommendationsAdapter: RecomendationsAdapter? = null
    var mSimilarAdapter: SimilarAdapter? = null
    var mVideosAdapter: VideosAdapter? = null
    var mCreditsAdapter: CreditsAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_activity)

        mToolbar = findViewById(R.id.toolbar_item)
        setSupportActionBar(mToolbar)

        initUI()
    }


    private fun initUI() {

        val actionbar = supportActionBar
        actionbar!!.setDisplayHomeAsUpEnabled(true)
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back_button)
        actionbar.title = getString(R.string.movie_desc)

        mImageView = findViewById(R.id.backdrop_item)
        mToolbar = findViewById(R.id.toolbar_item)
        mItemSlogan = findViewById(R.id.slogan_item)
        mItemOverview = findViewById(R.id.item_overview)
        mItemTitle = findViewById(R.id.title_item)
        mItemGenre = findViewById(R.id.genre)
        mItemReleaseDate = findViewById(R.id.release_date_item)
        mProgressBar = findViewById(R.id.progress_bar_item)
        mOriginalTitle = findViewById(R.id.item_original_title)
        mAvgVote = findViewById(R.id.item_vote_average)
        mRevenue = findViewById(R.id.item_revenue)
        mBudget = findViewById(R.id.item_budget)
        mRuntime = findViewById(R.id.item_runtime)
        mRecyclerView = findViewById(R.id.movies_list)
        mRecyclerViewSimilar = findViewById(R.id.movies_list_similar)
        mLogosRecyclerView = findViewById(R.id.movie_item_logos_recycler)
        mVideosRecyclerView = findViewById(R.id.video_recycler)
        mRecyclerViewCredits = findViewById(R.id.credits_recycler)

        mRecommendationsAdapter = RecomendationsAdapter(this)
        mRecyclerView!!.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL, false
        )
        mRecyclerView!!.adapter = mRecommendationsAdapter

        mVideosAdapter = VideosAdapter(this)
        mVideosRecyclerView!!.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL, false
        )
        mVideosRecyclerView!!.adapter = mVideosAdapter

        mSimilarAdapter = SimilarAdapter(this)
        mRecyclerViewSimilar!!.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL, false
        )
        mRecyclerViewSimilar!!.adapter = mSimilarAdapter

        mCreditsAdapter = CreditsAdapter()
        mRecyclerViewCredits!!.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL, false
        )
        mRecyclerViewCredits!!.adapter = mCreditsAdapter

        mLogosAdapter = LogosAdapter()
        mLogosRecyclerView!!.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL, false
        )
        mLogosRecyclerView!!.adapter = mLogosAdapter

        val movie_id = intent.getStringExtra("movie_id")

        val language = getSharedPreferences(
            getString(R.string.app_name),
            Context.MODE_PRIVATE
        ).getString("language", "en-EN")

        mLoader = MoviesLoader(this)
        mLoader!!.loadMovieDetails(movie_id, language!!)
        mLoader!!.loadMovieVideos(movie_id)
        mLoader!!.loadCastDetails(movie_id)

        mRecommendationsLoader = RecomendationsLoader(this)
        mRecommendationsLoader!!.loadRecommendedMovies(movie_id)
        mRecommendationsLoader = RecomendationsLoader(this)
        mRecommendationsLoader!!.loadSimilarMovies(movie_id)
    }

    override fun onMoviesLoaded(movies: List<Result>, genre_id: Int) {

    }

    override fun onMovieDetailsLoaded(movieDetails: Movie?) {
        mLogosAdapter!!.setLogos(movieDetails!!.productionCompanies!!)

        mProgressBar!!.visibility = View.GONE
        mItemSlogan!!.text = movieDetails.tagline!!
        mItemOverview!!.text = movieDetails.overview!!
        mItemGenre!!.text = movieDetails.originalLanguage!!
        mItemReleaseDate!!.text = movieDetails.releaseDate!!
        mItemTitle!!.text = movieDetails.title!!
        mOriginalTitle!!.text = getString(R.string.original_title) + movieDetails.title
        mRuntime!!.text = getString(R.string.Runtime) + movieDetails.runtime!! + "min"
        mRevenue!!.text = getString(R.string.Revenue) + movieDetails.revenue!! + "$"
        mBudget!!.text = getString(R.string.Budget) + movieDetails.budget!! + "$"
        mAvgVote!!.text = getString(R.string.average_vote) + movieDetails.voteAverage!!

        Picasso.get()
            .load(MovieService.IMAGE_ENDPOINT + movieDetails.posterPath!!)
            .into(mImageView)

    }


    override fun onMovieVideosLoaded(videoResult: List<Video>) {
        Toast.makeText(this, getString(R.string.videos_loaded) + videoResult.size, Toast.LENGTH_SHORT).show()
        this.mVideosAdapter!!.setmVideoResults(videoResult)
    }

    override fun onCastDetailsLoaded(casts: List<Cast>) = mCreditsAdapter!!.setmCasts(casts)

    override fun onRecommendationsLoaded(movies: List<Result>) = mRecommendationsAdapter!!.setMovies(movies)

    override fun onSimilarLoaded(movies: List<Result>) = mSimilarAdapter!!.setMovies(movies)
}
