package com.example.finalproject.activity


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.finalproject.R
import com.example.finalproject.model.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        private const val USER_INFO_COLLECTION = "user_info"
        fun start(context: Context) {
            context.startActivity(Intent(context,
                LoginActivity::class.java))
        }
    }

    private val firebaseAuth by lazy { FirebaseAuth.getInstance() }
    private val userInfoCollection by lazy { FirebaseFirestore.getInstance().collection(USER_INFO_COLLECTION) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val user = firebaseAuth.currentUser

        if (user != null) {
            //DetailsActivity.start(this)
            finish()
        }

        initUI()
    }

    private fun initUI() {
        sign_in_button.setOnClickListener(this)
        sign_up_button.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        val email = email_input.text.toString()
        val password = password_input.text.toString()

        when (view?.id) {
            R.id.sign_in_button -> {
                signIn(email, password)

            }
            R.id.sign_up_button -> {
                signUp(email, password)
            }
        }
    }

    private fun signUp(email: String, password: String) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
                task ->
            run {
                if (task.isSuccessful) {
                    addUserInfo(email)
                    Toast.makeText(this, getString(R.string.success_message),
                        Toast.LENGTH_LONG).show()
                } else {
                    val error = task.exception

                    Toast.makeText(this, error?.message,
                        Toast.LENGTH_LONG).show()
                }
            }
        }


    }

    private fun signIn(email: String, password: String) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                task ->
            run {
                if (task.isSuccessful) {
                    Toast.makeText(this, getString(R.string.success_message),
                        Toast.LENGTH_LONG).show()
                    //DetailsActivity.start(this)
                    finish()
                } else {
                    val error = task.exception

                    Toast.makeText(this, error?.message,
                        Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    private fun addUserInfo(email: String){
        val firstName = first_name_input.text.toString()
        val lastName = last_name_input.text.toString()
        val userInfo = UserInfo(email = email, firstName = firstName, lastName = lastName)

        userInfoCollection.add(userInfo).addOnCompleteListener {
                task ->
            run {
                if (task.isSuccessful) {
                    Toast.makeText(this, R.string.success_message,
                        Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, task.exception?.message,
                        Toast.LENGTH_LONG).show()
                }
            }
        }

    }

}