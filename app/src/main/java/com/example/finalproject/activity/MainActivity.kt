package com.example.finalproject.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.finalproject.R
import com.example.finalproject.adapter.ViewPagerAdapter
import com.example.finalproject.fragments.*

class MainActivity: AppCompatActivity(), View.OnClickListener {

    private var mTabLayout: TabLayout? = null
    private var mViewPager: ViewPager? = null
    private var mToolbar: android.support.v7.widget.Toolbar? = null

    val PREF_LANGUAGE = "language"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mToolbar = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbar)

        initUI()
    }


    private fun initUI() {
        mTabLayout = findViewById(R.id.tabLayout)
        mViewPager = findViewById(R.id.viewPager)

        val actionbar = supportActionBar
        actionbar!!.setDisplayHomeAsUpEnabled(true)
        actionbar.setHomeAsUpIndicator(R.drawable.ic_home)

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.AddFragment(PopularFragment(), getString(R.string.popular))
        adapter.AddFragment(TopRatedFragment(), getString(R.string.top_rated))
        adapter.AddFragment(NowPlayingFragment(), getString(R.string.now_playing))
        adapter.AddFragment(UpcomingFragment(), getString(R.string.upcoming))

        mViewPager!!.adapter = adapter
        mTabLayout!!.setupWithViewPager(mViewPager)

        val language = getSharedPreferences(
            getString(R.string.app_name),
            Context.MODE_PRIVATE
        ).getString(PREF_LANGUAGE, "en-EN")
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                Toast.makeText(
                    this, "Home ",
                    Toast.LENGTH_SHORT
                ).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.actionbar, menu)
        return true
    }

    override fun onClick(v: View) {

    }
}