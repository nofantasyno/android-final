package com.example.finalproject

import android.view.View
import com.example.finalproject.api.model.Genre
import com.example.finalproject.api.model.Movie

interface OnItemClickedListener {
    fun onMovieClicked(v: View, position: Int, isLongClick: Boolean)
    fun onGenreClicked(v: View, position: Int, isLongClick: Boolean)
}