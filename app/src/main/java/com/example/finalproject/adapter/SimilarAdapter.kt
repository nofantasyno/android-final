package com.example.finalproject.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.finalproject.OnItemClickedListener
import com.example.finalproject.R
import com.example.finalproject.activity.MovieActivity
import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.Movie
import com.squareup.picasso.Picasso
import com.example.finalproject.api.model.Result
import java.util.ArrayList

class SimilarAdapter()
    :RecyclerView.Adapter<SimilarAdapter.SimilarViewHolder>(){

    lateinit var mContext: Context
    lateinit var mMovies: MutableList<Result>

    constructor(mContext: Context) : this() {
        this.mContext = mContext
        mMovies = ArrayList()
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SimilarViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_item_movie, viewGroup, false)

        return SimilarViewHolder(view)
    }

    override fun onBindViewHolder(similarViewHolder: SimilarViewHolder, i: Int) {
        mMovies[i].let { similarViewHolder.bindMovie(it) }

        similarViewHolder.setOnItemClickedListenerListener(object : OnItemClickedListener {
            override fun onMovieClicked(v: View, position: Int, isLongClick: Boolean) {
                if (isLongClick) {
                } else {
                    val intent = Intent(mContext, MovieActivity::class.java)
                    intent.putExtra("movie_id", mMovies[position].id.toString())
                    mContext.startActivity(intent)
                }
            }

            override fun onGenreClicked(v: View, position: Int, isLongClick: Boolean) {

            }
        })
    }

    override fun getItemCount() = mMovies.size

    fun setMovies(movies: List<Result>) {
        mMovies.clear()
        mMovies.addAll(movies)

        notifyDataSetChanged()
    }

    inner class SimilarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
        View.OnLongClickListener {
        val mTitleView: TextView = itemView.findViewById(R.id.title_view)
        val mRatingView: TextView = itemView.findViewById(R.id.rating_view)
        val mReleaseDateView: TextView = itemView.findViewById(R.id.release_date_view)

        val mBackdrop: ImageView = itemView.findViewById(R.id.backdrop)

        var onItemClickedListenerListener: OnItemClickedListener? = null

        internal fun setOnItemClickedListenerListener(onItemClickedListenerListener: OnItemClickedListener) {
            this.onItemClickedListenerListener = onItemClickedListenerListener
        }

        init {

            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        fun bindMovie(result: Result) {
            mTitleView.text = result.title
            mRatingView.text = result.voteAverage.toString()
            mReleaseDateView.text = result.releaseDate

            Picasso.get()
                .load(MovieService.IMAGE_ENDPOINT + result.posterPath)
                .into(mBackdrop)
        }

        override fun onClick(v: View) {
            onItemClickedListenerListener!!.onMovieClicked(v, adapterPosition, false)
        }

        override fun onLongClick(v: View): Boolean {
            onItemClickedListenerListener!!.onMovieClicked(v, adapterPosition, true)

            return false
        }
    }
}