package com.example.finalproject.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.finalproject.OnItemClickedListener
import com.example.finalproject.R
import com.example.finalproject.activity.MovieActivity
import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.Result
import com.squareup.picasso.Picasso
import java.util.ArrayList

class RecomendationsAdapter()
    : RecyclerView.Adapter<RecomendationsAdapter.RecommendationsViewHolder>() {

    lateinit var mContext: Context
    lateinit var mMovies: MutableList<Result>

    constructor(mContext: Context) : this() {
        this.mContext = mContext
        mMovies = ArrayList()
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecommendationsViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_item_movie, viewGroup, false)

        return RecommendationsViewHolder(view)
    }

    override fun onBindViewHolder(recommendationsViewHolder: RecommendationsViewHolder, i: Int) {
        mMovies[i].let { recommendationsViewHolder.bindMovie(it) }

        recommendationsViewHolder.setOnItemClickedListenerListener(object : OnItemClickedListener {
            override fun onMovieClicked(v: View, position: Int, isLongClick: Boolean) {
                if (isLongClick) {
                    Toast.makeText(mContext, "LongClick: $position", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(
                        mContext,
                        "ShortClick: " + position + mMovies?.get(position)!!.id.toString(),
                        Toast.LENGTH_SHORT
                    ).show()

                    val intent = Intent(mContext, MovieActivity::class.java)
                    intent.putExtra("movie_id", mMovies?.get(position)!!.id.toString())
                    mContext.startActivity(intent)
                }
            }

            override fun onGenreClicked(v: View, position: Int, isLongClick: Boolean) {

            }
        })

    }

    override fun getItemCount() =  mMovies.size

    fun setMovies(movies: List<Result>) {
        mMovies.clear()
        mMovies.addAll(movies)

    }

    inner class RecommendationsViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

        private val mTitleView: TextView = itemView.findViewById(R.id.title_view)
        private val mRatingView: TextView = itemView.findViewById(R.id.rating_view)
        private val mReleaseDateView: TextView = itemView.findViewById(R.id.release_date_view)

        private val mBackdrop: ImageView = itemView.findViewById(R.id.backdrop)

        private var onItemClickedListenerListener: OnItemClickedListener? = null

        internal fun setOnItemClickedListenerListener(onItemClickedListenerListener: OnItemClickedListener) {
            this.onItemClickedListenerListener = onItemClickedListenerListener
        }

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        fun bindMovie(result: Result) {
            mTitleView.text = result.title
            mRatingView.text = result.voteAverage!!.toString()
            mReleaseDateView.text = result.releaseDate!!.toString()

            Picasso.get()
                .load(MovieService.IMAGE_ENDPOINT + result.posterPath!!)
                .into(mBackdrop)
        }

        override fun onClick(v: View) {
            onItemClickedListenerListener!!.onMovieClicked(v, adapterPosition, false)
        }

        override fun onLongClick(v: View): Boolean {
            onItemClickedListenerListener!!.onMovieClicked(v, adapterPosition, true)

            return true
        }
    }
}