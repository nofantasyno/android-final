package com.example.finalproject.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.finalproject.OnItemClickedListener
import com.example.finalproject.R
import com.example.finalproject.activity.MainActivity
import com.example.finalproject.api.model.Genre
import java.util.ArrayList


class GenreAdapter(val mContext: Context): RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {

    var mGenreView: TextView? = TextView(this.mContext)
    var genres: MutableList<Genre> = ArrayList()


    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): GenreAdapter.GenreViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_item_genre, viewGroup, false)

        return GenreViewHolder(view)
    }

    override fun onBindViewHolder(genreViewHolder: GenreAdapter.GenreViewHolder, i: Int) {
        genreViewHolder.bindGenre(genres[i])

        genreViewHolder.setOnItemClickedListener(object : OnItemClickedListener {
            override fun onMovieClicked(v: View, position: Int, isLongClick: Boolean) {

            }

            override fun onGenreClicked(v: View, position: Int, isLongClick: Boolean) {
                if (isLongClick) {
                } else {

                    val intent = Intent(mContext, MainActivity::class.java)
                    intent.putExtra("genre_id", genres.get(position).id.toString())
                    mContext.startActivity(intent)
                }
            }
        })
    }

    override fun getItemCount() = genres.size


    fun setGenreList(genres: ArrayList<Genre>) {
        genres.clear()
        genres.addAll(genres)

        notifyDataSetChanged()
    }

    inner class GenreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
        View.OnLongClickListener {

        private var mOnItemClickedListener: OnItemClickedListener? = null

        internal fun setOnItemClickedListener(mOnItemClickedListener: OnItemClickedListener) {
            this.mOnItemClickedListener = mOnItemClickedListener
        }

        init {
            mGenreView = itemView.findViewById<TextView>(R.id.genre_list_item)

            itemView.setOnClickListener(this)
        }

        fun bindGenre(genre: Genre) {
            mGenreView!!.setText(genre.name!!)
        }

        override fun onClick(v: View) {
            mOnItemClickedListener!!.onGenreClicked(v, adapterPosition, false)
        }

        override fun onLongClick(v: View): Boolean {
            mOnItemClickedListener!!.onGenreClicked(v, adapterPosition, true)
            return false
        }
    }

}