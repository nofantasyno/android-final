package com.example.finalproject.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.finalproject.R
import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.Cast
import com.squareup.picasso.Picasso
import java.util.ArrayList

class CreditsAdapter
    : RecyclerView.Adapter<CreditsAdapter.CreditsViewHolder>(){
    private var mCasts: MutableList<Cast> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CreditsAdapter.CreditsViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_item_cast, viewGroup, false)
        return CreditsViewHolder(view)
    }

    override fun onBindViewHolder(creditsViewHolder: CreditsAdapter.CreditsViewHolder, i: Int) {
        mCasts!![i].let { creditsViewHolder.bindCredit(it) }
    }

    override fun getItemCount() = mCasts!!.size

    fun setmCasts(Casts: List<Cast>) {
        mCasts!!.clear()
        mCasts!!.addAll(Casts)

        notifyDataSetChanged()
    }

    inner class CreditsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mTextView: TextView = itemView.findViewById(R.id.cast_character)
        private val mTextView2: TextView = itemView.findViewById(R.id.cast_name)
        private val mImageView: ImageView = itemView.findViewById(R.id.cast_backdrop)


        fun bindCredit(cast: Cast) {

            mTextView.text = cast.character
            Picasso.get()
                .load(MovieService.IMAGE_ENDPOINT + cast.profilePath)
                .into(mImageView)
            mTextView2.text = cast.name
        }
    }
}