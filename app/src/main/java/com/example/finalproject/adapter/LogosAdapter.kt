package com.example.finalproject.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.finalproject.R
import com.example.finalproject.api.MovieService
import com.example.finalproject.api.model.ProductionCompany
import com.example.finalproject.api.model.Result
import com.squareup.picasso.Picasso
import java.util.ArrayList

class LogosAdapter : RecyclerView.Adapter<LogosAdapter.LogosViewHolder>() {

    var mProductionCompanies: MutableList<ProductionCompany> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): LogosViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.production_company_layout, viewGroup, false)

        return LogosViewHolder(view)
    }

    override fun onBindViewHolder(logosViewHolder: LogosViewHolder, i: Int) {
        logosViewHolder.bindLogo(mProductionCompanies[i])
    }

    override fun getItemCount() = mProductionCompanies.size

    fun setLogos(productionCompanies: List<ProductionCompany>) {
        mProductionCompanies.clear()
        mProductionCompanies.addAll(productionCompanies)

        notifyDataSetChanged()
    }

    inner class LogosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val mTextView: TextView = itemView.findViewById(R.id.production_company_title)
        val mImageView: ImageView = itemView.findViewById(R.id.production_company_logo)

        fun bindLogo(productionCompany: ProductionCompany) {
            mTextView.text = productionCompany.name!!
            if(productionCompany.logoPath != null) {
                Picasso.get()
                    .load(MovieService.IMAGE_ENDPOINT + productionCompany.logoPath)
                    .into(mImageView)
            }
        }
    }
}
