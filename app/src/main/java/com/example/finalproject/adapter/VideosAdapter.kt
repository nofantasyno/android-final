package com.example.finalproject.adapter

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.finalproject.OnItemClickedListener
import com.example.finalproject.R
import com.example.finalproject.api.model.Video
import java.util.ArrayList

class VideosAdapter(val mContext: Context)
    : RecyclerView.Adapter<VideosAdapter.VideosViewHolder>(){
    var mVideoResults: MutableList<Video> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VideosAdapter.VideosViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_video_item, viewGroup, false)

        return VideosViewHolder(view)
    }

    override fun onBindViewHolder(videosViewHolder: VideosViewHolder, i: Int) {
        videosViewHolder.bindVideo(mVideoResults[i])

        videosViewHolder.setOnItemClickedListenerListener(object : OnItemClickedListener {
            override fun onMovieClicked(v: View, position: Int, isLongClick: Boolean) {
                if (!isLongClick) {
                    val appIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + mVideoResults[position].key))
                    val webIntent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://www.youtube.com/watch?v=" + mVideoResults[position].key)
                    )
                    try {
                        mContext.startActivity(appIntent)
                    } catch (ex: ActivityNotFoundException) {
                        mContext.startActivity(webIntent)
                    }

                } else {
                    Toast.makeText(mContext, "LongClick", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onGenreClicked(v: View, position: Int, isLongClick: Boolean) {

            }
        })

    }

    override fun getItemCount(): Int {
        return mVideoResults.size
    }

    fun setmVideoResults(videoResults: List<Video>) {
        mVideoResults.clear()
        mVideoResults.addAll(videoResults)

        notifyDataSetChanged()
    }

    inner class VideosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
        View.OnLongClickListener {

        val mTextView: TextView = itemView.findViewById(R.id.video_title)
        val mImageView: ImageView? = null

        var onItemClickedListenerListener: OnItemClickedListener? = null

        internal fun setOnItemClickedListenerListener(onItemClickedListenerListener: OnItemClickedListener) {
            this.onItemClickedListenerListener = onItemClickedListenerListener
        }


        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        fun bindVideo(videoResult: Video) {
            mTextView.text = videoResult.name
        }

        override fun onClick(v: View) {
            onItemClickedListenerListener!!.onMovieClicked(v, adapterPosition, false)
        }

        override fun onLongClick(v: View): Boolean {
            onItemClickedListenerListener!!.onMovieClicked(v, adapterPosition, true)

            return false
        }
    }
}