package com.example.finalproject.model

data class UserInfo(val email: String,
               val firstName: String,
               val lastName: String) {
    constructor(): this( "", "", "")
}