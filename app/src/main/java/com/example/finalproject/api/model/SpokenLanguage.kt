package com.example.finalproject.api.model

import com.google.gson.annotations.SerializedName

data class SpokenLanguage (
    @SerializedName("iso_639_1") val iso6391: String? = null,
    @SerializedName("name") val name: String? = null
)