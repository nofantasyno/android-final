package com.example.finalproject.api.model

import com.google.gson.annotations.SerializedName

data class Casts(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("cast") val cast: List<Cast>? = null,
    @SerializedName("crew") val crew: List<Crew>? = null
)