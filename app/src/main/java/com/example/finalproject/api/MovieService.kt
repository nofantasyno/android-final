package com.example.finalproject.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object MovieService {
    private val ENDPOINT = "https://api.themoviedb.org/3/"
    val IMAGE_ENDPOINT = "http://image.tmdb.org/t/p/w780/"

    fun getApi(): MovieAPI {
        return getRetrofit().create(MovieAPI::class.java)
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(ENDPOINT)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}