package com.example.finalproject.api.model

import com.google.gson.annotations.SerializedName

data class Videos(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("results") val results: List<Video>? = null
)