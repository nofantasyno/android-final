package com.example.finalproject.api.model

import com.google.gson.annotations.SerializedName

data class Genres(
    @SerializedName("genres") val genres: ArrayList<Genre>? = null
)