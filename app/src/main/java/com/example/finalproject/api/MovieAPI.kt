package com.example.finalproject.api

import com.example.finalproject.api.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieAPI {
    @GET("movie/popular?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getPopularMovies(@Query("language") language: String): Call<Movies>

    @GET("movie/top_rated?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getTopRatedMovies(@Query("language") language: String): Call<Movies>

    @GET("movie/upcoming?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getUpcomingMovies(@Query("language") language: String): Call<Movies>

    @GET("movie/latest?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getLatestMovie(): Call<Movie>

    @GET("movie/now_playing?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getNowPlayingMovies(@Query("language") language: String): Call<Movies>

    @GET("movie/{movie_id}?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getMovieDetails(@Path("movie_id") id: String, @Query("language") language: String): Call<Movie>

    @GET("movie/{movie_id}/credits?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getCastDetails(@Path("movie_id") id: String): Call<Casts>

    @GET("movie/{movie_id}/recommendations?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getMovieRecommendations(@Path("movie_id") id: String): Call<Recommendations>

    @GET("genre/movie/list?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getGenreList(): Call<Genres>

    @GET("movie/{movie_id}/similar?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getSimilarMovies(@Path("movie_id") id: String): Call<Recommendations>

    @GET("movie/{movie_id}/videos?api_key=531ba5c8b526ecef76eb479712858f46")
    fun getMovieVideos(@Path("movie_id") id: String): Call<Videos>
}